<?php
namespace Gamma\ITP\Block;

use \Magento\Framework\View\Element\Template;

class Index extends Template
{
    public function getTitle($value){
        return $this->_scopeConfig->getValue('itp/general/' . $value);
    }
}
